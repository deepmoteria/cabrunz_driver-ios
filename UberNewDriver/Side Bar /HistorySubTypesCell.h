//
//  HistorySubTypesCell.h
//  uHoo Driver
//
//  Created by My Mac on 6/18/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistorySubTypesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblMile;
@property (weak, nonatomic) IBOutlet UILabel *lblNewName;

@end
