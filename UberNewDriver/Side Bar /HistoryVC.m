//
//  HistoryVC.m
//  UberforX Provider
//
//  Created by My Mac on 11/15/14.
//  Copyright (c) 2014 Deep Gami. All rights reserved.
//

#import "HistoryVC.h"
#import "HistoryCell.h"
#import "UIImageView+Download.h"
#import "UtilityClass.h"
#import "PickMeUpMapVC.h"
#import "ArrivedMapVC.h"
#import "FeedBackVC.h"
#import "HistorySubTypesCell.h"
#import "UIView+Utils.h"

@interface HistoryVC ()
{
    NSMutableArray *arrHistory;
    NSMutableString *strUserId;
    NSMutableString *strUserToken;
    NSMutableArray *arrForDate;
    NSMutableArray *arrForSection,*arrSubType;
    NSString *strDistCost;

    BOOL internet;
}

@end

@implementation HistoryVC

@synthesize tableHistory,tableForSubTypes;


#pragma mark-
#pragma mark- View Delegate Method

- (void)viewDidLoad
{
    [super viewDidLoad];
    [super setBackBarItem];
    arrHistory=[[NSMutableArray alloc]init];
    [self customFont];
    [self localizeString];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    internet=[APPDELEGATE connected];
    [self.paymentView setHidden:YES];
    
    NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
    [pref synchronize];
    strUserId=[pref objectForKey:PREF_USER_ID];
    strUserToken=[pref objectForKey:PREF_USER_TOKEN];
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"LOADING_HISTORY", nil)];
    [self getHistory];
    self.imgNoItems.hidden=YES;
    self.navigationController.navigationBarHidden=NO;
    [self.btnMenu setTitle:NSLocalizedString(@"nav_History", nil) forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated
{
    //[self.btnMenu setTitle:NSLocalizedString(@"nav_History", nil) forState:UIControlStateNormal];
}

#pragma mark-
#pragma mark- customFont

-(void)customFont
{
//    self.lblBasePrice.font=[UberStyleGuide fontRegular];
//    self.lblDistCost.font=[UberStyleGuide fontRegular];
//    self.lblPerDist.font=[UberStyleGuide fontRegular];
//    self.lblPerTime.font=[UberStyleGuide fontRegular];
//    self.lblTimeCost.font=[UberStyleGuide fontRegular];
    self.lblTotal.font=[UberStyleGuide fontRegularBold:30.0f];
    
    //self.btnMenu.titleLabel.font=[UberStyleGuide fontRegularNav];
    self.btnClose=[APPDELEGATE setBoldFontDiscriptor:self.btnClose];
}

-(void)localizeString
{
    self.lblInvoice.text = NSLocalizedString(@"Invoice", nil);
    self.lblTime_Cost.text = NSLocalizedString(@"TIME COST", nil);
    self.lblDist_Cost.text = NSLocalizedString(@"DISTANCE COST", nil);
    self.lblTotalDue.text = NSLocalizedString(@"Total Due", nil);
    self.lblBasePrice.text = NSLocalizedString(@"BASE PRICE", nil);
    self.lbl_Promo.text = NSLocalizedString(@"PROMO BOUNCE", nil);
    self.lbl_Referrel.text = NSLocalizedString(@"REFERRAL BOUNCE", nil);
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateNormal];
    [self.btnClose setTitle:NSLocalizedString(@"CLOSE", nil) forState:UIControlStateHighlighted];
    
}

#pragma mark-
#pragma mark- Get History API Method

-(void)getHistory
{
    if(internet)
    {
        NSUserDefaults *pref=[NSUserDefaults standardUserDefaults];
        NSString * strForUserId=[pref objectForKey:PREF_USER_ID];
        NSString * strForUserToken=[pref objectForKey:PREF_USER_TOKEN];
        
        NSMutableString *pageUrl=[NSMutableString stringWithFormat:@"%@?%@=%@&%@=%@",FILE_HISTORY,PARAM_ID,strForUserId,PARAM_TOKEN,strForUserToken];
        AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
        [afn getDataFromPath:pageUrl withParamData:nil withBlock:^(id response, NSError *error)
         {
             NSLog(@"History Data= %@",response);
             
             [APPDELEGATE hideLoadingView];
             if (response)
             {
                 if([[response valueForKey:@"success"] intValue]==1)
                 {
                     arrHistory=[response valueForKey:@"requests"];
                     if (arrHistory.count==0)
                     {
                         self.imgNoItems.hidden=NO;
                         self.tableHistory.hidden=YES;
                     }
                     else
                     {
                         [self makeSection];
                         self.imgNoItems.hidden=YES;
                         self.tableHistory.hidden=NO;
                         [tableHistory reloadData];
                         //[tableForSubTypes reloadData];
                     }
                 }
             }
         }];
    }
    else
    {
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:NSLocalizedString(@"No Internet", nil) message:NSLocalizedString(@"NO_INTERNET", nil) delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

#pragma mark-
#pragma mark- Table View Delegate

-(void)makeSection
{
    arrForDate=[[NSMutableArray alloc]init];
    arrForSection=[[NSMutableArray alloc]init];
    NSMutableArray *arrtemp=[[NSMutableArray alloc]init];
    [arrtemp addObjectsFromArray:arrHistory];
    NSSortDescriptor *distanceSortDiscriptor = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:NO
                                                                              selector:@selector(localizedStandardCompare:)];
    [arrtemp sortUsingDescriptors:@[distanceSortDiscriptor]];
    
    for (int i=0; i<arrtemp.count; i++)
    {
        NSMutableDictionary *dictDate=[[NSMutableDictionary alloc]init];
        dictDate=[arrtemp objectAtIndex:i];
        
        NSString *temp=[dictDate valueForKey:@"date"];
        NSArray *arrDate=[temp componentsSeparatedByString:@" "];
        NSString *strdate=[arrDate objectAtIndex:0];
        if(![arrForDate containsObject:strdate])
        {
            [arrForDate addObject:strdate];
        }
    }
    
    for (int j=0; j<arrForDate.count; j++)
    {
        NSMutableArray *a=[[NSMutableArray alloc]init];
        [arrForSection addObject:a];
    }
    for (int j=0; j<arrForDate.count; j++)
    {
        NSString *strTempDate=[arrForDate objectAtIndex:j];
        
        for (int i=0; i<arrtemp.count; i++)
        {
            NSMutableDictionary *dictSection=[[NSMutableDictionary alloc]init];
            dictSection=[arrtemp objectAtIndex:i];
            NSArray *arrDate=[[dictSection valueForKey:@"date"] componentsSeparatedByString:@" "];
            NSString *strdate=[arrDate objectAtIndex:0];
            if ([strdate isEqualToString:strTempDate])
            {
                [[arrForSection objectAtIndex:j] addObject:dictSection];
            }
        }
        
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    if(tableView.tag==1)
    {
        return arrForSection.count;
    }
    else
    {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return  [[arrForSection objectAtIndex:section] count];
    }
    else
    {
        return arrSubType.count;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(tableView.tag == 1)
    {
        return 20.0f;
    }
    else
    {
        return 0.0f;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    
    if(tableView.tag == 1)
    {
        return 1.0;
    }
    else
    {
        return 0.f;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if(tableView.tag==1)
    {
        UIView *headerView=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 20)];
        UILabel *lblDate=[[UILabel alloc]initWithFrame:CGRectMake(20, 2, 300, 15)];
        lblDate.font=[UberStyleGuide fontRegular:13.0f];
        lblDate.textColor=[UIColor blackColor];
        NSString *strDate=[arrForDate objectAtIndex:section];
        NSString *current=[[UtilityClass sharedObject] DateToString:[NSDate date] withFormate:@"yyyy-MM-dd"];
        
        UILabel *lblfoot=[[UILabel alloc]initWithFrame:CGRectMake(0, 19, 320,1)];
        lblfoot.backgroundColor=[UIColor blackColor];
        [headerView addSubview:lblfoot];
        
        ///   YesterDay Date Calulation
        
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = -1;
        NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                       toDate:[NSDate date]
                                                      options:0];
        NSString *strYesterday=[[UtilityClass sharedObject] DateToString:yesterday withFormate:@"yyyy-MM-dd"];
        
        if([strDate isEqualToString:current])
        {
            lblDate.text=NSLocalizedString(@"TODAY", nil);
            headerView.backgroundColor=[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];
            lblDate.textColor=[UIColor blackColor];
        }
        else if ([strDate isEqualToString:strYesterday])
        {
            lblDate.text=NSLocalizedString(@"YESTERDAY",nil);
            headerView.backgroundColor=[UIColor grayColor];
            lblDate.textColor=[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];
        }
        else
        {
            NSDate *date=[[UtilityClass sharedObject]stringToDate:strDate withFormate:@"yyyy-MM-dd"];
            NSString *text=[[UtilityClass sharedObject]DateToString:date withFormate:@"dd MMMM yyyy"];//2nd Jan 2015
            lblDate.text=text;
            headerView.backgroundColor=[UIColor grayColor];
            lblDate.textColor=[UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];
        }
        
        [headerView addSubview:lblDate];
        return headerView;
    }
    else
    {
        return nil;
    }

}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    /*if(tableView.tag==1)
    {
        UIImageView *imgFooter=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"rectangle2"]];
        return imgFooter;
    }
    else
    {
        return nil;
    }*/
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag==1)
    {
        static NSString *CellIdentifier = @"Cell";
    
        HistoryCell *cell = [tableHistory dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        
        if (cell==nil)
        {
            cell=[[HistoryCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        NSMutableDictionary *dictOwner=[pastDict valueForKey:@"owner"];

        cell.lblName.font=[UberStyleGuide fontRegular:16.0f];
        cell.lblDateTime.font=[UberStyleGuide fontRegular:16.0f];
        cell.lblCost.font=[UberStyleGuide fontRegular:20.0f];
        cell.lblType.font=[UberStyleGuide fontRegular];
    
        NSDate *dateTemp=[[UtilityClass sharedObject]stringToDate:[pastDict valueForKey:@"date"]];
        NSString *strDate=[[UtilityClass sharedObject]DateToString:dateTemp withFormate:@"hh:mm a"];
        cell.lblDateTime.text=[NSString stringWithFormat:@"%@",strDate];
        //cell.lblDateTime.font=[UberStyleGuide fontRegular];
        cell.lblName.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"full_name"]];
        cell.lblType.text=[NSString stringWithFormat:@"%@",[dictOwner valueForKey:@"phone"]];
        cell.lblCost.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"total"] floatValue]];
    
        [cell.imgOwner downloadFromURL:[dictOwner valueForKey:@"picture"] withPlaceholder:nil];
        [cell.imgOwner applyRoundedCornersFullWithColor:[UIColor clearColor]];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1)];
        
        //lineView.backgroundColor = [UIColor grayColor];
        lineView.backgroundColor = [UIColor colorWithRed:98.0f/255.0f green:254.0f/255.0f blue:241.0f/255.0f alpha:1];
        [cell.contentView addSubview:lineView];

    
        return cell;
    }
    
    else
    {
        static NSString *cellIdentifier = @"sub_types";
        
        HistorySubTypesCell *cell = [tableForSubTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell==nil)
        {
            cell=[[HistorySubTypesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *dict = [arrSubType objectAtIndex:indexPath.row];
        //cell.lblName.text = [dict valueForKey:@"name"];
        //cell.lblPrice.font = [UberStyleGuide fontRegularBold:12.0f];
        cell.lblPrice.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
        
        
        cell.lblName.hidden = YES;
        //cell.lblNewName.hidden = YES;
        cell.lblMile.hidden = YES;
        ////cell.lblPrice.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
        //cell.lblMile.text = [[arrSubType objectAtIndex:2]valueForKey:@"time_cost_per"];
        cell.lblNewName.text = [dict valueForKey:@"name"];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1)];
        
        //lineView.backgroundColor = [UIColor grayColor];
        lineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"line-1"]];
        [cell.contentView addSubview:lineView];
        
        /*if(indexPath.row == 1 || indexPath.row == 2)
        {
            cell.lblMile.hidden = YES;
            cell.lblName.hidden = YES;
            cell.lblNewName.hidden =YES;
        }*/
//        else
//        {
//            cell.lblMile.hidden = YES;
//        }
        
        /*if(indexPath.row == 0)
        {
            cell.lblName.hidden = YES;
            //cell.lblNewName.hidden = YES;
            cell.lblMile.hidden = YES;
            cell.lblPrice.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
            //cell.lblMile.text = [[arrSubType objectAtIndex:1]valueForKey:@"distance_cost_per"];
            cell.lblNewName.text = [dict valueForKey:@"name"];
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1)];
            
            //lineView.backgroundColor = [UIColor grayColor];
            lineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"line-1"]];
            [cell.contentView addSubview:lineView];
        }
        else if(indexPath.row == 1)
        {
            cell.lblName.hidden = YES;
            //cell.lblNewName.hidden = YES;
            cell.lblMile.hidden = YES;
            ////cell.lblPrice.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
            //cell.lblMile.text = [[arrSubType objectAtIndex:2]valueForKey:@"time_cost_per"];
            cell.lblNewName.text = [dict valueForKey:@"name"];
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1)];
            
            //lineView.backgroundColor = [UIColor grayColor];
            lineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"line-1"]];
            [cell.contentView addSubview:lineView];
        }
        else if(indexPath.row == 2)
        {
            cell.lblName.hidden = YES;
            //cell.lblNewName.hidden = NO;
            cell.lblNewName.text = [dict valueForKey:@"name"];
            cell.lblMile.hidden = YES;
            //cell.lblPrice.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];
            UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, cell.contentView.frame.size.width, 1)];
            
            //lineView.backgroundColor = [UIColor grayColor];
            lineView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"line-1"]];
            [cell.contentView addSubview:lineView];
        }*/
        
        return cell;

    }
    
    /*if(tableView==tableForSubTypes)
    {
        static NSString *cellIdentifier = @"sub_types";
        
        HistorySubTypesCell *cell = [tableForSubTypes dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
        
        if (cell==nil)
        {
            cell=[[HistorySubTypesCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        }
        
        NSDictionary *dict = [self.arrSubTypes objectAtIndex:indexPath.row];
        cell.lblName.text = [dict valueForKey:@"name"];
        cell.lblPrice.text =[NSString stringWithFormat:@"$%@", [dict valueForKey:@"price"]];

    }*/
    
   // return NO;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView.tag == 1)
        return 60;
    else
        return 45;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    /*NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    arrSubType = [pastDict valueForKey:@"subtype"];
    
    NSArray *arrTemp=[[NSArray alloc]initWithArray:arrSubType];
    arrSubType = [[NSMutableArray alloc]init];*/
    
    if(tableView.tag==1)
    {
        self.navigationController.navigationBarHidden=YES;
        [tableView deselectRowAtIndexPath:indexPath animated:NO];
        
        NSMutableDictionary *pastDict=[[arrForSection objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        arrSubType = [pastDict valueForKey:@"subtype"];
        
        NSArray *arrTemp=[[NSArray alloc]initWithArray:arrSubType];
        arrSubType = [[NSMutableArray alloc]init];
        
        NSMutableDictionary *dictname = [[NSMutableDictionary alloc]init];
        self.lblTotal.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"total"] floatValue]];
        [dictname setObject:NSLocalizedString(@"Base Price", nil) forKey:@"name"];
        [dictname setObject:[pastDict valueForKey:@"base_price"] forKey:@"price"];
        [arrSubType addObject:dictname];
        
        NSMutableDictionary *dictService = [[NSMutableDictionary alloc]init];
        [dictService setObject:@"Serivce Cost" forKey:@"name"];
        [dictService setObject:[pastDict valueForKey:@"service_cost"] forKey:@"price"];
        [arrSubType addObject:dictService];
        
        /*dictname = [[NSMutableDictionary alloc]init];
        [dictname setObject:NSLocalizedString(@"Distance Cost", nil) forKey:@"name"];
        [dictname setObject:[pastDict valueForKey:@"distance_cost"] forKey:@"price"];
        
        float totalDist=[[pastDict valueForKey:@"distance_cost"] floatValue];
        float Dist=[[pastDict valueForKey:@"distance"]floatValue];
        
        if(Dist!=0)
        {
            strDistCost =[NSString stringWithFormat:@"%.2f$ %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
        }
        else
        {
            strDistCost=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mile", nil)];
        }
        [dictname setObject:strDistCost forKey:@"distance_cost_per"];
        [arrSubType addObject:dictname];
        
        dictname = [[NSMutableDictionary alloc]init];
        [dictname setObject:NSLocalizedString(@"Time Cost", nil) forKey:@"name"];
        [dictname setObject:[pastDict valueForKey:@"time_cost"] forKey:@"price"];
        
        float totalTime=[[pastDict valueForKey:@"time_cost"] floatValue];
        float Time=[[pastDict valueForKey:@"time"]floatValue];
        if(Time!=0)
        {
            strDistCost=[NSString stringWithFormat:@"%.2f$ %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
        }
        else
        {
            strDistCost=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mins", nil)];
        }
        
        [dictname setObject:strDistCost forKey:@"time_cost_per"];
        [arrSubType addObject:dictname];
        [arrSubType addObjectsFromArray:arrTemp];*/
        
        NSMutableDictionary *dictprice = [[NSMutableDictionary alloc]init];
        
        [dictprice setObject:NSLocalizedString(@"Promo Bonus", nil) forKey:@"name"];
        [dictprice setObject:[pastDict valueForKey:@"promo_bonus"] forKey:@"price"];
        [arrSubType addObject:dictprice];
        
        NSMutableDictionary *dictReferral = [[NSMutableDictionary alloc]init];
        [dictReferral setObject:NSLocalizedString(@"Referral Bonus", nil) forKey:@"name"];
        [dictReferral setObject:[pastDict valueForKey:@"referral_bonus"] forKey:@"price"];
        [arrSubType addObject:dictReferral];
        tableForSubTypes.tag=2;
        [tableForSubTypes reloadData];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.paymentView.hidden=NO;
        } completion:^(BOOL finished)
         {
         }];
    }
    else
    {
        
    }

    
    /*self.lblBasePrice.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"base_price"] floatValue]];
    self.lblDistCost.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"distance_cost"] floatValue]];
    self.lblTimeCost.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"time_cost"] floatValue]];
    self.lblTotal.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"total"] floatValue]];
    self.lblPromo.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"promo_bonus"] floatValue]];
    self.lblReferrel.text=[NSString stringWithFormat:@"$%.2f",[[pastDict valueForKey:@"referral_bonus"] floatValue]];
    
    
    float totalDist=[[pastDict valueForKey:@"distance_cost"] floatValue];
    float Dist=[[pastDict valueForKey:@"distance"]floatValue];
    
    if ([[pastDict valueForKey:@"unit"]isEqualToString:NSLocalizedString(@"kms", nil)])
    {
        totalDist=totalDist*0.621317;
        Dist=Dist*0.621371;
    }
    if(Dist!=0)
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"%.2f$ %@",(totalDist/Dist),NSLocalizedString(@"per mile", nil)];
    }
    else
    {
        self.lblPerDist.text=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mile", nil)];
    }
    
    float totalTime=[[pastDict valueForKey:@"time_cost"] floatValue];
    float Time=[[pastDict valueForKey:@"time"]floatValue];
    if(Time!=0)
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"%.2f$ %@",(totalTime/Time),NSLocalizedString(@"per mins", nil)];
    }
    else
    {
        self.lblPerTime.text=[NSString stringWithFormat:@"0$ %@",NSLocalizedString(@"per mins", nil)];
    }
        
    

    [self.paymentView setHidden:NO];*/
        

}



#pragma mark-
#pragma mark- Button Method


- (IBAction)backBtnPressed:(id)sender
{
    NSArray *currentControllers = self.navigationController.viewControllers;
    NSMutableArray *newControllers = [NSMutableArray
                                      arrayWithArray:currentControllers];
    UIViewController *obj=nil;
    
    for (int i=0; i<newControllers.count; i++)
    {
        UIViewController *vc=[self.navigationController.viewControllers objectAtIndex:i];
        if ([vc isKindOfClass:[FeedBackVC class]])
        {
            obj = (FeedBackVC *)vc;
        }
        else if ([vc isKindOfClass:[ArrivedMapVC class]])
        {
            obj = (ArrivedMapVC *)vc;
        }
        else if ([vc isKindOfClass:[PickMeUpMapVC class]])
        {
            obj = (PickMeUpMapVC *)vc;
        }
        
    }
    [self.navigationController popToViewController:obj animated:YES];
    //[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)closeBtnPressed:(id)sender
{
    self.navigationController.navigationBarHidden=NO;
    [self.paymentView setHidden:YES];
    self.tableHistory.tag=1;
    
}
@end
