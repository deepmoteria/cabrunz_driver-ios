//
//  SubTypesCell.h
//  uHoo Driver
//
//  Created by My Mac on 6/17/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubTypesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblSubTypeNm;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTypePrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPerMile;
@property (weak, nonatomic) IBOutlet UILabel *lblNewName;

@end
