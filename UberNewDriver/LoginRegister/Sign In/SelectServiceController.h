//
//  SelectServiceController.h
//  Device Wizard Provider
//
//  Created by My Mac on 7/22/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import "BaseVC.h"
#import "SelectServiceCell.h"

@interface SelectServiceController : BaseVC
@property (weak, nonatomic) IBOutlet UITableView *tableForService;
@property(strong,nonatomic)NSMutableDictionary *dictRegister;
@property(strong,nonatomic)UIImage *imgProfile;
@property (weak, nonatomic) IBOutlet UIButton *btnMenu;

- (IBAction)onClickforRegister:(id)sender;
@end
