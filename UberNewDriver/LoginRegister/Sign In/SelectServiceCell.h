//
//  SelectServiceCell.h
//  Device Wizard Provider
//
//  Created by My Mac on 7/22/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectServiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblServiceName;
@property (weak, nonatomic) IBOutlet UIImageView *lblServiceImg;

@end
