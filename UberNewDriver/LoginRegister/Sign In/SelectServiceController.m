//
//  SelectServiceController.m
//  Device Wizard Provider
//
//  Created by My Mac on 7/22/15.
//  Copyright (c) 2015 Deep Gami. All rights reserved.
//

#import "SelectServiceController.h"

@interface SelectServiceController ()
{
    NSMutableArray *arrTypes,*arrForTypesId;
}

@end

@implementation SelectServiceController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setBackBarItem];
    arrTypes = [[NSMutableArray alloc]init];
    arrForTypesId = [[NSMutableArray alloc]init];
    NSLog(@"register details :%@",self.dictRegister);
    NSLog(@"image  :%@",self.imgProfile);
    [self.btnMenu setTitle:@"Select services you do" forState:UIControlStateNormal];
    [APPDELEGATE hideLoadingView];
    
    [self getType];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getType
{
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:GET_METHOD];
    [afn getDataFromPath:FILE_WALKER_TYPE withParamData:nil withBlock:^(id response, NSError *error)
     {
         if (response) {
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [[AppDelegate sharedAppDelegate]hideHUDLoadingView];
                 arrTypes=[response valueForKey:@"types"];
                 NSLog(@"arr :%@",arrTypes);
                 [self.tableForService reloadData];
                 //NSLog(@"Check %@",response);
             }
         }
         
     }];

}

#pragma mark tableview

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return arrTypes.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict=[arrTypes objectAtIndex:indexPath.row];
    
    SelectServiceCell *cell=(SelectServiceCell *)[tableView dequeueReusableCellWithIdentifier:@"SelectCell"];
    cell.lblServiceName.text = [dict valueForKey:@"name"];
    cell.lblServiceName.textColor = [UIColor grayColor];
    return cell;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 49.0f;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SelectServiceCell *Cell=(SelectServiceCell *)[self.tableForService cellForRowAtIndexPath:indexPath];
    
    if([Cell.lblServiceImg.image isEqual:[UIImage imageNamed:@"btn_uncheck"]])
    {
        NSDictionary *dictType=[arrTypes objectAtIndex:indexPath.row];
        NSString *strTypeId=[dictType valueForKey:@"id"];
        [arrForTypesId addObject:strTypeId];
        Cell.lblServiceImg.image=[UIImage imageNamed:@"btn_check"];
        Cell.lblServiceName.textColor = [UIColor whiteColor];
        Cell.backgroundColor = [UIColor colorWithRed:16.0f/255.0f green:16.0f/255.0f blue:16.0f/255.0f alpha:1];
    }
    else
    {
        Cell.lblServiceImg.image=[UIImage imageNamed:@"btn_uncheck"];
        Cell.lblServiceName.textColor = [UIColor grayColor];
        Cell.backgroundColor = [UIColor clearColor];
        
        /* mansi code
        
         NSString *name = [[arrForSelectProps objectAtIndex:indexPath.row] valueForKey:@"name"];
         NSString *subTypeId = [[arrForSelectProps objectAtIndex:indexPath.row] valueForKey:@"id"];
         SelectPropsCellVC *cell = (SelectPropsCellVC*)[tableView cellForRowAtIndexPath:indexPath];
         if([self isRowSelectedOnTableView:tableView atIndexPath:indexPath]){
         [arrForIndex removeObject:indexPath];
         [arrForSelected removeObject:name];
         [arrForSubTypeId removeObject:subTypeId];
         NSLog(@" remov element%@",arrForSelected);
         cell.imgSelectedCell.hidden = YES;
         cell.lblProps.font=[UberStyleGuide fontRegularLightBig];
         [cell setBackgroundColor:[UIColor whiteColor]];
         } else {
         [arrForIndex addObject:indexPath];
         [arrForSelected addObject:name];
         [arrForSubTypeId addObject:subTypeId];
         
         NSLog(@"added elememt = %@",arrForSelected);
         cell.imgSelectedCell.hidden = NO;
         [cell setBackgroundColor:[UIColor colorWithRed:241.0f/255.0f green:241.0f/255.0f blue:241.0f/255.0f alpha:1]];
         cell.lblProps.font = [UberStyleGuide fontRegularLightBigBold];
         
         }
         [self.revealViewController rightRevealToggle:nil];
         
         [self.tblForProps deselectRowAtIndexPath:indexPath animated:YES];
         NSLog(@"%@",arrForIndex);
         
         */

    }

    [self.tableForService deselectRowAtIndexPath:indexPath animated:YES];
    
}

/*-(void)btnCommentClick:(id)sender
{
    UIButton *senderButton = (UIButton *)sender;
    NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
    SelectServiceCell *cell=[self.tableForService cellForRowAtIndexPath:path];
    
    
    //NSLog(@"text :%@",cell.lbltitle.text);
    
    // cell.btnPrice.backgroundColor=[UIColor colorWithPatternImage:[UIImage imageNamed:@"bg_price"]];
    
    //[self.tblselectService reloadData];
    
    /*
     UIButton *senderButton = (UIButton *)sender;
     
     NSLog(@"current Row=%ld",(long)senderButton.tag);
     // NSIndexPath *path = [NSIndexPath indexPathForRow:senderButton.tag inSection:0];
     NSLog(@" sdsd %@" ,senderButton.titleLabel.text);
 
}
*/

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)onClickforRegister:(id)sender
{
    [APPDELEGATE showLoadingWithTitle:NSLocalizedString(@"SIGN_IN", nil)];
    if(arrForTypesId.count==0)
    {
        [arrForTypesId addObject:@"1"];
    }
    
    NSString *joinedStringprice12 = [arrForTypesId componentsJoinedByString:@","];
    
    /*for(int i=0;i<arrPricet.count;i++)
    {
        NSString *Str=[arrPricet objectAtIndex:i];
        int val=[[arrPricet objectAtIndex:i] intValue];
        if(val<=0 || [Str isEqualToString:@""])
        {
            [arrPricet removeObject:[arrPricet objectAtIndex:i]];
        }
    }
    
    NSString *joinedStringprice11 = [arrPricet componentsJoinedByString:@","];
    NSString *joinedStringprice12 = [arrWtype componentsJoinedByString:@","];
    
    NSLog(@"price2   :%@",joinedStringprice11);
    NSLog(@"type    :%@",joinedStringprice12);*/
    
    [self.dictRegister setValue:joinedStringprice12 forKey:PARAM_WALKER_TYPE];
    //[self.dictparam setValue:joinedStringprice11 forKey:PARAM_WALKER_PRICE];
    
    AFNHelper *afn=[[AFNHelper alloc]initWithRequestMethod:POST_METHOD];
    [afn getDataFromPath:FILE_REGISTER withParamDataImage:self.dictRegister andImage:self.imgProfile withBlock:^(id response, NSError *error)
     {
         //[APPDELEGATE hideLoadingView];
         if (response)
         {
             [APPDELEGATE hideLoadingView];
             arrUser=response;
             if([[response valueForKey:@"success"] intValue]==1)
             {
                 [APPDELEGATE showToastMessage:(NSLocalizedString(@"REGISTER_SUCCESS", nil))];
                 //   arrUser=response;
                 NSLog(@"res :%@",response);
                 [self.navigationController popToRootViewControllerAnimated:YES];
             }
             else
             {
                 UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"" message:[response valueForKey:@"error"] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil, nil];
                 [alert show];
             }
         }
         NSLog(@"REGISTER RESPONSE --> %@",response);
     }];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
